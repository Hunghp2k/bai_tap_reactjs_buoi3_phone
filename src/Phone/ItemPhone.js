import React, { Component } from 'react'

export default class ItemPhone extends Component {
    render() {
        let { hinhAnh, giaBan, tenSP } = this.props.data
        return (
            <div className='col-4 p-4'>
                <div className="card border-primary">
                    <img className="card-img-top" src={hinhAnh} alt />
                    <div className="card-body">
                        <h4 className="card-title">{tenSP}</h4>
                        <p className="card-text">{giaBan}</p>
                        <button onClick={() => { this.props.detailSP(this.props.data) }} className='btn btn-success'>Xem Chi Tiết</button>
                    </div>

                </div>
            </div>
        )
    }
}
