import React, { Component } from 'react'
import { data_phone } from './data_phone'
import DetailPhone from './DetailPhone'
import ListPhone from './ListPhone'

export default class Phone extends Component {
    state = {
        listPhone: data_phone,
        detail: data_phone[0],
    }
    detailSP = (SP) => {
        this.setState({ detail: SP })
    }
    render() {
        return (
            <div>
                <ListPhone detailSP={this.detailSP} list={this.state.listPhone} />
                <DetailPhone detail={this.state.detail} />
            </div>
        )
    }
}
